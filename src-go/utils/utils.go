package utils

import (
	"encoding/hex"
	"errors"
	"math/big"
	"strings"
)

const CreditsBinPath = "./data/credits.bin"
const CreditsCsvPath = "./data/credits.csv"

const AddressBinLen = 20
const MaxCreditBinLen = 32
const MinRecordLen = AddressBinLen + 1               // Excludes the record length prefix byte
const MaxRecordLen = AddressBinLen + MaxCreditBinLen // Excludes the record length prefix byte

const WeiPerEth = 1e18

var weiPerEthFloat = big.NewFloat(WeiPerEth)

func PanicFail(e error) {
	if e != nil {
		panic(e)
	}
}

func HexToBinAddr(s string) ([]byte, error) {
	if len(s) != 2+2*AddressBinLen || !strings.HasPrefix(s, "0x") {
		return nil, errors.New("invalid")
	}
	return hex.DecodeString(s[2:])
}

func HexToBinCredit(s string) ([]byte, error) {
	n := len(s)
	if n <= 2 || n > 2+2*MaxCreditBinLen || !strings.HasPrefix(s, "0x") {
		return nil, errors.New("invalid")
	}
	s = s[2:]
	if n%2 == 1 {
		s = "0" + s
	}
	return hex.DecodeString(s)
}

func BinToHexAddr(bytes []byte) string {
	return "0x" + hex.EncodeToString(bytes)
}

func BinToHexCredit(bytes []byte) string {
	s := hex.EncodeToString(bytes)
	if s[0] == '0' {
		s = s[1:]
	}
	return "0x" + s
}

func WeiToEth(wei *big.Int) *big.Float {
	weiFloat := new(big.Float).SetInt(wei)
	return new(big.Float).Quo(weiFloat, weiPerEthFloat)
}
