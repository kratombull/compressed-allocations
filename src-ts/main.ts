import * as path from 'path';

import * as yargs from 'yargs';
import BigNumber from 'bignumber.js';

import { formatAuditDate, appendAudit, AuditEntry, mapAppend } from './audit';
import {
  getSacrificeDay,
  calcSacrificeCredits,
  BonusType,
  getBonusType,
  calcFlatBonus,
  calcVolumeBonus,
  getAuditDate,
  setSacrificeDay0,
} from './bonus';
import { exportCreditsCsv, exportBonusAudit, exportRawReducedCsv } from './export-credits';
import { exportRawCsv } from './export-raw';
import { dedupSourcePrices, filterUndesirables, importRawCsv } from './import-raw';
import { Address, Sacrifice, SacrificeMap, Point } from './types';

BigNumber.config({
  DECIMAL_PLACES: 100,
  EXPONENTIAL_AT: 101,
});

const argv = yargs.options({
  project: {
    type: 'string',
    default: '.',
    describe: 'the subfolder to use as the input',
  },
  startTime: {
    type: 'string',
    default: '2021-07-15T04:49:00.000Z',
    describe: 'the point in time that is the zero date',
  },
});

const args = argv.parseSync();

const IMPORT_RAW_CSV_NAME = 'raw.csv';
const EXPORT_RAW_ADVERTISED_FOR_CSV_NAME = 'raw-advertised-for.csv';
const EXPORT_RAW_IGNORED_CSV_NAME = 'raw-ignored.csv';
const EXPORT_RAW_NOT_IGNORED_CSV_NAME = 'raw-not-ignored.csv';
const EXPORT_CREDITS_CSV_NAME = 'credits.csv';
const EXPORT_POINT_EVENT_NAME = 'point-event.csv';
const EXPORT_RAW_REDUCED_NAME = 'raw-reduced.csv';
const EXPORT_RAW_SALES_CSV_NAME = 'sales.csv';

const DATA_DIR = './data';
const RAW_CSV_PATH = path.join(DATA_DIR, args.project, IMPORT_RAW_CSV_NAME);
const RAW_ADVERTISED_FOR_CSV_PATH = path.join(DATA_DIR, args.project, EXPORT_RAW_ADVERTISED_FOR_CSV_NAME);
const RAW_NOT_IGNORED_CSV_PATH = path.join(DATA_DIR, args.project, EXPORT_RAW_NOT_IGNORED_CSV_NAME);
const RAW_IGNORED_CSV_PATH = path.join(DATA_DIR, args.project, EXPORT_RAW_IGNORED_CSV_NAME);
const RAW_SALES_CSV_PATH = path.join(DATA_DIR, args.project, EXPORT_RAW_SALES_CSV_NAME);
const CREDITS_CSV_PATH = path.join(DATA_DIR, args.project, EXPORT_CREDITS_CSV_NAME);
const POINT_EVENT_PATH = path.join(DATA_DIR, args.project, EXPORT_POINT_EVENT_NAME);
const RAW_REDUCED_CSV_PATH = path.join(DATA_DIR, args.project, EXPORT_RAW_REDUCED_NAME);

setSacrificeDay0(new Date(args.startTime));

main();

function dollarValueOfSacrifices(sacrifices: Sacrifice[]): BigNumber {
  return sacrifices.reduce((memo, sac) => memo.plus(
    (new BigNumber(sac.amount.toString()))
      .dividedBy(`1e${sac.decimals}`)
      .times(sac.usdPrice.toString())
      .dividedBy(1e36),
  ), new BigNumber(0));
}

function main() {
  const sacrifices = importRawCsv(RAW_CSV_PATH);

  const { categories } = dedupSourcePrices(sacrifices);

  const keyToPath = new Map<string, string>([
    ['advertisedFor', RAW_ADVERTISED_FOR_CSV_PATH],
    ['notIgnoring', RAW_NOT_IGNORED_CSV_PATH],
    ['ignoring', RAW_IGNORED_CSV_PATH],
    ['sales', RAW_SALES_CSV_PATH],
  ]);
  const used = [
    'advertisedFor',
    // 'notIgnoring',
    // 'ignoring',
    'sales',
  ];
  // write out all files
  [...keyToPath.entries()].map(([key, filePath]) => (
    exportRawCsv(filePath, [...(categories.get(key) as SacrificeMap).values()])
  ));

  const accepted = used.reduce((memo, key) => (
    new Map<string, Sacrifice>([...memo, ...categories.get(key) as Map<string, Sacrifice>])
  ), new Map<string, Sacrifice>());
  const acceptedAsList = filterUndesirables([...accepted.values()]);
  const summaries = [...categories.keys()].map((key) => {
    const category = categories.get(key) as Map<string, Sacrifice>;
    return `${category.size} ${key} for ($${dollarValueOfSacrifices([...category.values()]).toString()})`;
  });
  console.info(`Imported ${sacrifices.length} Sacrifices
  ${summaries.join('\n  ')}

${accepted.size} accepted: ${used.join(' + ')}`);

  const { addrPoints, addrAudits: _, pointBreakdown } = allocatePoints(acceptedAsList);

  exportBonusAudit(POINT_EVENT_PATH, pointBreakdown);
  exportRawReducedCsv(RAW_REDUCED_CSV_PATH, sacrifices);
  exportCreditsCsv(CREDITS_CSV_PATH, addrPoints);
  console.info(`Exported ${addrPoints.size} Addresses`);
}

function allocatePoints(sacrifices: Sacrifice[]) {
  const addrPoints = new Map<Address, bigint>();
  const addrAudits = new Map<Address, AuditEntry[]>();
  const txPoints: (Sacrifice & { credits: bigint })[] = [];
  const pointBreakdown = new Map<Address, Point[]>();

  let totalVolume = 0n;
  const addrVolume = new Map<Address, bigint>();

  for (const s of sacrifices) {
    let bonus = 0n;
    const addr = s.creditAddress;
    const ts = +s.minedTimestamp;
    const day = getSacrificeDay(ts);
    const auditDate = getAuditDate(ts);
    const date = formatAuditDate(auditDate);

    // turns the amount into a decimal, which is what price calculated set against
    const decimalFactor = BigInt(`1${'0'.repeat(s.decimals)}`);
    const scaledUsdValue = (s.amount * s.usdPrice) / decimalFactor; // 18 decimals x 18 decimals = 36 decimals

    let credits = calcSacrificeCredits(scaledUsdValue, day, s.isSens);

    appendAudit(addrAudits, addr, { date, type: 'credit', value: scaledUsdValue, points: credits });
    switch (getBonusType(day)) {
      case BonusType.VOLUME:
        addPoints(addrVolume, addr, credits);
        totalVolume += credits;
        break;

      case BonusType.FLAT: {
        bonus = calcFlatBonus(credits);
        appendAudit(addrAudits, addr, { date, type: 'flat_bonus', value: credits, points: bonus });
        credits += bonus;
        break;
      }

      default: // BonusType.NONE
        break;
    }
    addPoints(addrPoints, addr, credits);

    mapAppend(pointBreakdown, addr, {
      network: s.network,
      hash: s.transactionHash,
      currency: s.currency,
      type: 'sacrifice',
      value: credits,
    });

    if (bonus) {
      mapAppend(pointBreakdown, addr, {
        network: s.network,
        hash: s.transactionHash,
        currency: s.currency,
        type: 'flat_bonus',
        value: bonus,
      });
    }
  }

  // Flatten map to an array, and sort by volume
  const ascendingVolumes = [...addrVolume]
    .sort((
      [_addrA, volumeA],
      [_addrB, volumeB],
    ) => (
      volumeA === volumeB
        ? 0
        : (volumeA < volumeB ? -1 : +1)
    ));

  const dateNow = formatAuditDate(new Date(0));

  let cumulativeVolumeBelow = 0n;
  for (const [addr, volume] of ascendingVolumes) {
    const bonus = calcVolumeBonus(cumulativeVolumeBelow, volume, totalVolume);

    appendAudit(addrAudits, addr, { date: dateNow, type: 'volume_bonus', value: volume, points: bonus });
    addPoints(addrPoints, addr, bonus);

    mapAppend(pointBreakdown, addr, {
      type: 'volume_bonus',
      value: bonus,
    });

    cumulativeVolumeBelow += volume;
  }

  return { txPoints, addrPoints, addrAudits, pointBreakdown };
}

function addPoints(m: Map<Address, bigint>, addr: Address, points: bigint) {
  const existingPoints = m.get(addr) || 0n;
  m.set(addr, existingPoints + points);
}
