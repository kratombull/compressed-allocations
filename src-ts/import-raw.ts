import * as fs from 'fs';

import { NetworkRuleMap, ruleForNetworks } from './rules';
import * as Rules from './rules';
import {
  PropertiesAsStrings,
  SacrificeFields,
  Sacrifice,
  Networks,
  Sources,
  SourcesList,
  SourceRankMap,
  rankForSources,
  Address,
} from './types';
import { scaleToDecimals18 } from './utils';
import {
  validateHeader,
  validateFieldCount,
  validateFieldType,
  validateField,
  validateAddrRemap,
  InvalidRowError,
} from './validation';

type SacrificeRow = PropertiesAsStrings<SacrificeFields>;

const TX_HASH_RULE_MAP: NetworkRuleMap = {
  'sens': Rules.TXHASH_SENS,

  ...ruleForNetworks(Rules.HASH_256_0X_LC, [
    'avalanche',
    'binancesmartchain',
    'ethereum',
    'ethereumclassic',
    'ethereum-optimism',
    'polygon',
    'theta',
    'zksync',
  ]),

  ...ruleForNetworks(Rules.HASH_256_LC, [
    'bitcoin',
    'bitcoin-cash',
    'cardano',
    'dogecoin',
    'eos',
    'litecoin',
    'monero',
    'stellar',
    'tron',
    'zcash',
  ]),

  ...ruleForNetworks(Rules.HASH_256_UC, [
    'binancechain',
    'ripple',
  ]),
};

const SOURCE_RANK_MAP: SourceRankMap = {
  ...rankForSources(0, SourcesList, 'sales/'),
  ...rankForSources(1, SourcesList, 'dexs/'),
  ...rankForSources(2, SourcesList, 'ccxt/'),
  ...rankForSources(3, SourcesList, 'dollarpegged'),
  ...rankForSources(4, SourcesList, 'coingecko'),
};

// Special exceptions of credit addresses that need to be re-mapped
const ADDR_REMAP = validateAddrRemap({
  '0xf2b1f3b06dcf2778707c257cfe387af3a587272b': '0xfb6d55200b1b113db222acd59a281ba5a4112530',
});

export function importRawCsv(rawCsvPath: string): Sacrifice[] {
  const text = fs.readFileSync(rawCsvPath, 'utf-8');
  const rows = text.split(/\r\n|\r|\n/);

  validateHeader(rawCsvPath, rows.shift());

  const sacrifices = rows.map((row, rowInd) => importRawCsvRow(rawCsvPath, row, rowInd));

  applyInterventions(sacrifices);

  return sacrifices;
}

export function importRawCsvRow(rawCsvPath: string, row: string, rowInd: number): Sacrifice {
  const f = row.split(',');

  try {
    validateFieldCount(f.length);

    const network = validateFieldType(f, 4, Networks);
    const source = validateFieldType(f, 9, Sources);
    const ticker = f[7];

    const s: SacrificeRow = {
      minedTimestamp: validateField(f, 0, Rules.ISO_DATETIME),
      transactionHash: validateField(f, 1, TX_HASH_RULE_MAP[network]),
      creditAddressId: validateField(f, 2, Rules.NONZERO_INTEGER),
      isSens: validateField(f, 3, Rules.BOOLEAN),
      network,
      blockId: validateField(f, 5, Rules.NONZERO_INTEGER),
      currency: validateField(f, 6, Rules.CURRENCY),
      ticker,
      decimals: validateField(f, 8, Rules.POSITIVE_INTEGER),
      source,
      creditAddress: validateField(f, 10, Rules.HASH_160_0X_LC),
      advertisedFor: validateField(f, 11, Rules.BOOLEAN),
      ignore: validateField(f, 12, Rules.BOOLEAN),
      amount: validateField(f, 13, Rules.NONZERO_INTEGER),
      usdPrice: validateField(f, 14, Rules.NONZERO_DECIMAL),
    };

    return {
      minedTimestamp: new Date(s.minedTimestamp),
      transactionHash: s.transactionHash,
      creditAddressId: +s.creditAddressId,
      isSens: s.isSens === 'true',
      network,
      blockId: +s.blockId,
      currency: s.currency,
      decimals: +s.decimals,
      source,
      creditAddress: s.creditAddress,
      advertisedFor: s.advertisedFor === 'true',
      ignore: s.ignore === 'true',
      amount: scaleToDecimals18(s.amount),
      usdPrice: scaleToDecimals18(s.usdPrice),
      ticker: s.ticker,

      sourceRank: SOURCE_RANK_MAP[source],
      rawRow: row,
    };
  } catch (err) {
    if (err instanceof InvalidRowError) {
      err.addContext(rawCsvPath, row, rowInd);
    }
    throw err;
  }
}

function applyInterventions(sacrifices: Sacrifice[]) {
  const toAddrSet = new Set(ADDR_REMAP.values());

  const toAddrIdMap = new Map<Address, number>();
  const updating = [] as Sacrifice[];

  for (const s of sacrifices) {
    const addr = s.creditAddress;

    if (ADDR_REMAP.has(addr)) {
      updating.push(s);
    } else if (toAddrSet.has(addr)) {
      toAddrIdMap.set(addr, s.creditAddressId);
    }
  }

  for (const s of updating) {
    const fromAddr = s.creditAddress;
    const fromAddrId = s.creditAddressId;

    const toAddr = ADDR_REMAP.get(fromAddr) as string; // Exists due to above test
    const toAddrId = toAddrIdMap.get(toAddr) || fromAddrId;

    s.creditAddress = toAddr;
    s.creditAddressId = toAddrId;

    console.log(`INTERVENTION: ${s.transactionHash}: ${fromAddr} (${fromAddrId}) -> ${toAddr} (${toAddrId})`);
  }
}

export function dedupSourcePrices(sacrifices: Sacrifice[]) {
  const couldAccept = new Map<string, Sacrifice>();
  const ignoringSet = new Set<string>();
  const notIgnoringSet = new Set<string>();
  const advertisedForSet = new Set<string>();
  const salesSet = new Set<string>();
  for (const s of sacrifices) {
    const key = sacrificeToKey(s);
    if (s.advertisedFor) {
      advertisedForSet.add(key);
    } else if (!s.ignore) {
      notIgnoringSet.add(key);
    } else {
      ignoringSet.add(key);
    }
    if (!s.sourceRank) {
      salesSet.add(key);
    }
    const existing = couldAccept.get(key);
    if (!existing || (s.sourceRank < existing.sourceRank && s.usdPrice > 0n)) {
      couldAccept.set(key, s);
    }
  }
  const advertisedFor = setToMap(couldAccept, advertisedForSet);
  const notIgnoring = setToMap(couldAccept, notIgnoringSet);
  const ignoring = setToMap(couldAccept, ignoringSet);
  const sales = setToMap(couldAccept, salesSet);
  const categories = new Map<string, Map<string, Sacrifice>>([
    ['advertisedFor', advertisedFor],
    ['notIgnoring', notIgnoring],
    ['ignoring', ignoring],
    ['sales', sales],
  ]);
  return {
    categories,
    couldAccept,
    advertisedFor,
    notIgnoring,
    ignoring,
    sales,
  };
}

const setToMap = (map: Map<string, Sacrifice>, set: Set<string>): Map<string, Sacrifice> => (
  new Map([...set.values()].map((key) => ([key, map.get(key) as Sacrifice])))
);

export const filterUndesirables = (sacrifices: Sacrifice[]): Sacrifice[] => (
  // only not ignored, and ignored sacrifices
  // with a source rank of "0" or "sale"
  // will be able to get through this filter
  sacrifices.filter((sacrifice) => (!sacrifice.ignore || !sacrifice.sourceRank))
);

function sacrificeToKey(s: Sacrifice): string {
  return `${s.transactionHash}-${s.network}-${s.currency}-${s.blockId}-${s.creditAddress}`;
}
